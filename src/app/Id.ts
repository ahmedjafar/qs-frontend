export interface Id {
    id: number;
}

export interface IAnswerId {
    Question_Id: number;
    Answer_Id: number;
}
