"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var admin_component_1 = require("./admin.component");
var quizzesList_component_1 = require("../quiz/quizzesList.component");
var quizCreator_component_1 = require("../quiz/quizCreator.component");
var quizEditor_component_1 = require("../quiz/quizEditor.component");
var questionEditor_component_1 = require("../question/questionEditor.component");
var allAssignedQuizzes_Component_1 = require("../quiz/allAssignedQuizzes.Component");
var adminRoutes = [
    { path: '', component: admin_component_1.AdminComponent,
        children: [
            { path: '', redirectTo: '/admin/quizzesList', pathMatch: 'full' },
            { path: 'quizzesList', component: quizzesList_component_1.QuizzesListComponent },
            { path: 'quizCreator', component: quizCreator_component_1.QuizCreatorComponent },
            { path: 'quizEditor/:id', component: quizEditor_component_1.QuizEditorComponent },
            { path: 'questionEditor/:quiz_Id', component: questionEditor_component_1.QuestionEditorComponent },
            { path: 'allAssignedQuizzes', component: allAssignedQuizzes_Component_1.AllAssignedQuizzesComponent },
        ],
    }
];
var AdminRoutingModule = (function () {
    function AdminRoutingModule() {
    }
    return AdminRoutingModule;
}());
AdminRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forChild(adminRoutes)],
        exports: [router_1.RouterModule]
    })
], AdminRoutingModule);
exports.AdminRoutingModule = AdminRoutingModule;
//# sourceMappingURL=admin-routing.module.js.map