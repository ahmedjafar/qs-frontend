import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AdminComponent } from './admin.component';
import { QuizzesListComponent } from '../quiz/quizzesList.component';
import { QuizCreatorComponent } from '../quiz/quizCreator.component';
import { QuizEditorComponent } from '../quiz/quizEditor.component';
import { QuestionEditorComponent } from '../question/questionEditor.component';
import { AllAssignedQuizzesComponent } from '../quiz/allAssignedQuizzes.Component';

const adminRoutes: Routes = [
    { path: '', component: AdminComponent,
        children:
        [
            { path: '', redirectTo: '/admin/quizzesList', pathMatch: 'full'},
            { path: 'quizzesList', component: QuizzesListComponent },
            { path: 'quizCreator', component: QuizCreatorComponent },
            { path: 'quizEditor/:id', component: QuizEditorComponent },
            { path: 'questionEditor/:quiz_Id', component: QuestionEditorComponent },
            { path: 'allAssignedQuizzes', component: AllAssignedQuizzesComponent },
        ],
    }
  ];

@NgModule({
    imports: [ RouterModule.forChild(adminRoutes) ],
    exports: [ RouterModule ]
})
export class AdminRoutingModule { }
