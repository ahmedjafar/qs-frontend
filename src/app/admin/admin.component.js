"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var status_reporting_service_1 = require("../status-reporting.service");
var AdminComponent = (function () {
    function AdminComponent(_statusReportingService, _router) {
        var _this = this;
        this._statusReportingService = _statusReportingService;
        this._router = _router;
        _statusReportingService.errorRaised$.subscribe(function (errorMessage) {
            _this.statusMessage = errorMessage;
            _this.showStatus = true;
            _this.statusClass = 'alert alert-danger';
        });
        _statusReportingService.successReported$.subscribe(function (successMessage) {
            _this.statusMessage = successMessage;
            _this.showStatus = true;
            _this.statusClass = 'alert alert-success';
        });
        _statusReportingService.removeStatusReported$.subscribe(function (message) {
            _this.statusMessage = message;
            _this.showStatus = false;
        });
    }
    AdminComponent.prototype.logOut = function () {
        localStorage.clear();
        // localStorage.removeItem('token');
        // localStorage.removeItem('role');
        // localStorage.removeItem('userId');
        this._router.navigate(['/login']);
    };
    return AdminComponent;
}());
AdminComponent = __decorate([
    core_1.Component({
        selector: 'admin-component',
        templateUrl: 'app/admin/admin.Component.html'
    }),
    __metadata("design:paramtypes", [status_reporting_service_1.StatusReportingService, router_1.Router])
], AdminComponent);
exports.AdminComponent = AdminComponent;
//# sourceMappingURL=admin.component.js.map