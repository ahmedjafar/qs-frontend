import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StatusReportingService } from '../status-reporting.service';

@Component({
    selector: 'admin-component',
    templateUrl: 'app/admin/admin.Component.html'
})
export class AdminComponent {
    showStatus: boolean;
    statusMessage: string;
    statusClass: string;

    constructor(private _statusReportingService: StatusReportingService, private _router:  Router) {
        _statusReportingService.errorRaised$.subscribe(
            errorMessage => {
              this.statusMessage = errorMessage;
              this.showStatus = true;
              this.statusClass = 'alert alert-danger';
            });

        _statusReportingService.successReported$.subscribe(
            successMessage => {
                this.statusMessage = successMessage;
                this.showStatus = true;
                this.statusClass = 'alert alert-success';
            });

        _statusReportingService.removeStatusReported$.subscribe(
            message => {
                this.statusMessage = message;
                this.showStatus = false;
            });
    }

    logOut(): void {
        localStorage.clear();
        // localStorage.removeItem('token');
        // localStorage.removeItem('role');
        // localStorage.removeItem('userId');
        this._router.navigate(['/login']);
    }
}
