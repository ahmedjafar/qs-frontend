"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var common_1 = require("@angular/common");
var admin_routing_module_1 = require("./admin-routing.module");
var http_1 = require("@angular/http");
var admin_component_1 = require("./admin.component");
var quizzesList_component_1 = require("../quiz/quizzesList.component");
var quizCreator_component_1 = require("../quiz/quizCreator.component");
var quizEditor_component_1 = require("../quiz/quizEditor.component");
var questionEditor_component_1 = require("../question/questionEditor.component");
var allAssignedQuizzes_Component_1 = require("../quiz/allAssignedQuizzes.Component");
var AdminModule = (function () {
    function AdminModule() {
    }
    return AdminModule;
}());
AdminModule = __decorate([
    core_1.NgModule({
        imports: [forms_1.FormsModule, common_1.CommonModule, admin_routing_module_1.AdminRoutingModule, http_1.HttpModule],
        declarations: [admin_component_1.AdminComponent, quizzesList_component_1.QuizzesListComponent, quizCreator_component_1.QuizCreatorComponent, quizEditor_component_1.QuizEditorComponent, questionEditor_component_1.QuestionEditorComponent,
            allAssignedQuizzes_Component_1.AllAssignedQuizzesComponent]
    })
], AdminModule);
exports.AdminModule = AdminModule;
//# sourceMappingURL=admin.module.js.map