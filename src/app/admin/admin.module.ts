import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { AdminRoutingModule } from './admin-routing.module';
import { HttpModule } from '@angular/http';

import { AdminComponent } from './admin.component';
import { QuizzesListComponent } from '../quiz/quizzesList.component';
import { QuizCreatorComponent } from '../quiz/quizCreator.component';
import { QuizEditorComponent } from '../quiz/quizEditor.component';
import { QuestionEditorComponent } from '../question/questionEditor.component';
import { AllAssignedQuizzesComponent } from '../quiz/allAssignedQuizzes.Component';

@NgModule({
    imports: [ FormsModule, CommonModule, AdminRoutingModule, HttpModule ],
    declarations: [ AdminComponent, QuizzesListComponent, QuizCreatorComponent, QuizEditorComponent, QuestionEditorComponent,
                    AllAssignedQuizzesComponent]

})
export class AdminModule { }
