"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_2 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/Observable/throw");
var AnswerService = (function () {
    function AnswerService(_http) {
        this._http = _http;
    }
    AnswerService.prototype.getAnswerIds = function (userId, quizId, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get("http://localhost:9768/api/AnswerIds?userid=" + userId + "&quizid=" + quizId, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    AnswerService.prototype.getAnswer = function (id, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get("http://localhost:9768/api/answers/" + id, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    AnswerService.prototype.postAnswer = function (answer, token) {
        answer = this.prepareAnswerForSave(answer);
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.post("http://localhost:9768/api/answers/", answer, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    AnswerService.prototype.updateAnswer = function (id, answer, token) {
        answer = this.prepareAnswerForSave(answer);
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.put("http://localhost:9768/api/answers/" + id, answer, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    AnswerService.prototype.prepareAnswerForSave = function (answer) {
        if (answer.Choice1 == null) {
            answer.Choice1 = false;
            console.log(1);
        }
        if (answer.Choice2 == null) {
            answer.Choice2 = false;
            console.log(2);
        }
        if (answer.Choice3 == null) {
            answer.Choice3 = false;
            console.log(3);
        }
        if (answer.Choice4 == null) {
            console.log(5);
            answer.Choice4 = false;
        }
        return answer;
    };
    AnswerService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error);
    };
    return AnswerService;
}());
AnswerService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_2.Http])
], AnswerService);
exports.AnswerService = AnswerService;
//# sourceMappingURL=answer.service.js.map