import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { IAnswerId } from '../Id';
import { IAnswer } from './answer';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';

@Injectable()
export class AnswerService {
    constructor(private _http: Http) { }

    getAnswerIds(userId: string, quizId: number, token: string): Observable<IAnswerId[]> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/AnswerIds?userid=${userId}&quizid=${quizId}` , options)
                         .map((response) => <IAnswerId[]>response.json())
                         .catch(this.handleError);
    }

    getAnswer(id: number, token: string): Observable<IAnswer> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/answers/${id}`, options)
                         .map((response) => <IAnswer>response.json())
                         .catch(this.handleError);
    }

    postAnswer(answer: IAnswer, token: string): Observable<IAnswer> {
        answer = this.prepareAnswerForSave(answer);
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.post(`http://localhost:9768/api/answers/`, answer, options)
                         .map((response) => <IAnswer>response.json())
                         .catch(this.handleError);
    }

    updateAnswer(id: number, answer: IAnswer, token: string): Observable<IAnswer> {
        answer = this.prepareAnswerForSave(answer);
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.put(`http://localhost:9768/api/answers/${id}`, answer, options)
                         .map((response) => <IAnswer>response.json())
                         .catch(this.handleError);
    }

    private prepareAnswerForSave(answer: IAnswer): IAnswer {
        if (answer.Choice1 == null) {
            answer.Choice1 = false;
            console.log(1);
        }
        if (answer.Choice2 == null) {
            answer.Choice2 = false;
            console.log(2);
        }
        if (answer.Choice3 == null) {
            answer.Choice3 = false;
            console.log(3);
        }
        if (answer.Choice4 == null) {
            console.log(5);
            answer.Choice4 = false;
        }

        return answer;
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}
