export interface IAnswer {
    Id: number;
    Question_Id: number;
    Choice1: boolean;
    Choice2: boolean;
    Choice3: boolean;
    Choice4: boolean;
    User_Id: string;
    Quiz_Id: number;
}

export class Answer implements IAnswer {
    Id: number;
    Question_Id: number;
    Choice1: boolean;
    Choice2: boolean;
    Choice3: boolean;
    Choice4: boolean;
    User_Id: string;
    Quiz_Id: number;

    constructor() { }
}
