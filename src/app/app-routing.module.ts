import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NotFoundComponent }  from './not-found.component';
import { UserLoginComponent } from './user/userLogin/userLogin.component';
import { AppGuard } from './app.guard';

const AppRoutes: Routes = [
  { path: 'admin', loadChildren: 'app/admin/admin.module#AdminModule', canActivate: [ AppGuard ] },
  { path: 'testee', loadChildren: 'app/testee/testee.module#TesteeModule', canActivate: [ AppGuard ] },
  { path: 'login', component: UserLoginComponent },
  { path: '', redirectTo: 'login', pathMatch: 'full'},
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(AppRoutes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
