import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

@Injectable()
export class AppGuard implements CanActivate {
    private _tokenStorageKey: string = 'token';

    constructor(private _router: Router) {}

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let role: string = localStorage.getItem('role');
        let pathPrefix: string = route.routeConfig.path;

        if (role && pathPrefix.toLowerCase() === role.toLowerCase() && localStorage.getItem(this._tokenStorageKey)) {
            return true;
        }

        this._router.navigate(['/login'], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
