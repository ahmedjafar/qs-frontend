"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var forms_1 = require("@angular/forms");
var common_2 = require("@angular/common");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var admin_module_1 = require("./admin/admin.module");
var testee_module_1 = require("./testee/testee.module");
var app_routing_module_1 = require("./app-routing.module");
var app_component_1 = require("./app.component");
var not_found_component_1 = require("./not-found.component");
var userLogin_component_1 = require("./user/userLogin/userLogin.component");
var user_service_1 = require("./user/user.service");
var status_reporting_service_1 = require("./status-reporting.service");
var quiz_service_1 = require("./quiz/quiz.service");
var question_service_1 = require("./question/question.service");
var answer_service_1 = require("./answer/answer.service");
var quizScore_service_1 = require("./quiz/quizScore.service");
var score_service_1 = require("./quiz/score.service");
var app_guard_1 = require("./app.guard");
var quizViewer_guard_1 = require("./quiz/quizViewer.guard");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [common_2.CommonModule, platform_browser_1.BrowserModule, forms_1.FormsModule, app_routing_module_1.AppRoutingModule, http_1.HttpModule, admin_module_1.AdminModule, testee_module_1.TesteeModule],
        declarations: [app_component_1.AppComponent, not_found_component_1.NotFoundComponent, userLogin_component_1.UserLoginComponent],
        bootstrap: [app_component_1.AppComponent],
        providers: [common_1.DatePipe, user_service_1.UserService, status_reporting_service_1.StatusReportingService, quiz_service_1.QuizService, question_service_1.QuestionService, answer_service_1.AnswerService, quizScore_service_1.QuizScoreService,
            score_service_1.ScoreService, app_guard_1.AppGuard, quizViewer_guard_1.QuizViewerGuard]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map