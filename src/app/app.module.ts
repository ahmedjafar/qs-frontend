import { NgModule } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import  { AdminModule } from './admin/admin.module';
import  { TesteeModule } from './testee/testee.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent }  from './app.component';

import { NotFoundComponent }  from './not-found.component';
import { UserLoginComponent } from './user/userLogin/userLogin.component';

import { UserService } from './user/user.service';
import { StatusReportingService } from './status-reporting.service';
import { QuizService } from './quiz/quiz.service';
import { QuestionService } from './question/question.service';
import { AnswerService } from './answer/answer.service';
import { QuizScoreService } from './quiz/quizScore.service';
import { ScoreService } from './quiz/score.service';
import { AppGuard } from './app.guard';
import { QuizViewerGuard } from './quiz/quizViewer.guard';

@NgModule({
  imports:      [ CommonModule, BrowserModule, FormsModule, AppRoutingModule, HttpModule, AdminModule, TesteeModule ],
  declarations: [ AppComponent, NotFoundComponent, UserLoginComponent ],
  bootstrap:    [ AppComponent ],
  providers:    [ DatePipe, UserService, StatusReportingService, QuizService, QuestionService, AnswerService, QuizScoreService,
                  ScoreService, AppGuard, QuizViewerGuard ]
})
export class AppModule { }
