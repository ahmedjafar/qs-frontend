import { Component } from '@angular/core';

@Component({
    selector: 'not-found',
    template: '<h1>Requested View Not Found!</h1>'
})
export class NotFoundComponent { }
