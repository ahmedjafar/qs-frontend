"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_2 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/Observable/throw");
var QuestionService = (function () {
    function QuestionService(_http) {
        this._http = _http;
    }
    QuestionService.prototype.getQuestionIds = function (quizId, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get("http://localhost:9768/api/questionIds/" + quizId, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuestionService.prototype.getQuestion = function (questionId, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get("http://localhost:9768/api/questions/" + questionId, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuestionService.prototype.getQuestionSecured = function (questionId, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get("http://localhost:9768/api/questions?questionId=" + questionId, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuestionService.prototype.createQuestion = function (question, token) {
        question = this.prepareQuestionForSave(question);
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.post("http://localhost:9768/api/questions/", question, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuestionService.prototype.updateQuestion = function (id, question, token) {
        question = this.prepareQuestionForSave(question);
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.put("http://localhost:9768/api/questions/" + id, question, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuestionService.prototype.deleteQuestion = function (id, token) {
        var headers = new http_2.Headers();
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.delete("http://localhost:9768/api/questions/" + id, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuestionService.prototype.prepareQuestionForSave = function (question) {
        if (question.IsChoice1Correct == null) {
            question.IsChoice1Correct = false;
            console.log(1);
        }
        if (question.IsChoice2Correct == null) {
            question.IsChoice2Correct = false;
            console.log(2);
        }
        if (question.IsChoice3Correct == null && question.Choice3 != null) {
            question.IsChoice3Correct = false;
            console.log(3);
        }
        else if ((question.IsChoice3Correct === true || question.IsChoice3Correct === false) && question.Choice3 == null) {
            console.log(question.Choice3);
            question.IsChoice3Correct = null;
            console.log(4);
        }
        if (question.IsChoice4Correct == null && question.Choice4 != null) {
            console.log(5);
            question.IsChoice4Correct = false;
        }
        else if ((question.IsChoice4Correct === true || question.IsChoice4Correct === false) && question.Choice4 == null) {
            console.log(6);
            question.IsChoice3Correct = null;
        }
        return question;
    };
    QuestionService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error);
    };
    return QuestionService;
}());
QuestionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_2.Http])
], QuestionService);
exports.QuestionService = QuestionService;
//# sourceMappingURL=question.service.js.map