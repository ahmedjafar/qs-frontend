import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { Id } from '../Id';
import { IQuestion } from './question';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';

@Injectable()
export class QuestionService {
    constructor(private _http: Http) { }

    getQuestionIds(quizId: number, token: string): Observable<Id[]> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/questionIds/${quizId}`, options)
                         .map((response) => <Id[]>response.json())
                         .catch(this.handleError);
    }

    getQuestion(questionId: number, token: string): Observable<IQuestion> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/questions/${questionId}`, options)
                         .map((response) => <IQuestion>response.json())
                         .catch(this.handleError);
    }

    getQuestionSecured(questionId: number, token: string): Observable<IQuestion> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/questions?questionId=${questionId}`, options)
                         .map((response) => <IQuestion>response.json())
                         .catch(this.handleError);
    }

    createQuestion(question: IQuestion, token: string): Observable<IQuestion> {
        question = this.prepareQuestionForSave(question);
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.post(`http://localhost:9768/api/questions/`, question, options)
                         .map((response) => <IQuestion>response.json())
                         .catch(this.handleError);
    }

    updateQuestion(id: number, question: IQuestion, token: string): Observable<IQuestion> {
        question = this.prepareQuestionForSave(question);
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.put(`http://localhost:9768/api/questions/${id}`, question, options)
                         .map((response) => <IQuestion>response.json())
                         .catch(this.handleError);
    }

    deleteQuestion(id:  number, token: string): Observable<Response> {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.delete(`http://localhost:9768/api/questions/${id}`, options)
                         .map((response) => response.json())
                         .catch(this.handleError);
    }

    private prepareQuestionForSave(question: IQuestion): IQuestion {
        if (question.IsChoice1Correct == null) {
            question.IsChoice1Correct = false;
            console.log(1);
        }
        if (question.IsChoice2Correct == null) {
            question.IsChoice2Correct = false;
            console.log(2);
        }
        if (question.IsChoice3Correct == null && question.Choice3 != null) {
            question.IsChoice3Correct = false;
            console.log(3);
        } else if ((question.IsChoice3Correct === true || question.IsChoice3Correct === false) && question.Choice3 == null) {
            console.log(question.Choice3);
            question.IsChoice3Correct = null;
            console.log(4);
        }
        if (question.IsChoice4Correct == null && question.Choice4 != null) {
            console.log(5);
            question.IsChoice4Correct = false;
        } else if ((question.IsChoice4Correct === true || question.IsChoice4Correct === false) && question.Choice4 == null) {
            console.log(6);
            question.IsChoice3Correct = null;
        }

        return question;
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}
