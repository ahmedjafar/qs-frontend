export interface IQuestion {
    Id: number;
    QuestionText: string;
    Choice1: string;
    Choice2: string;
    Choice3: string;
    Choice4: string;
    IsChoice1Correct: boolean;
    IsChoice2Correct: boolean;
    IsChoice3Correct: boolean;
    IsChoice4Correct: boolean;
    Quiz_Id: number;
}

export class Question implements IQuestion {
    Id: number;
    QuestionText: string;
    Choice1: string;
    Choice2: string;
    Choice3: string;
    Choice4: string;
    IsChoice1Correct: boolean;
    IsChoice2Correct: boolean;
    IsChoice3Correct: boolean;
    IsChoice4Correct: boolean;
    Quiz_Id: number;

    constructor() { }
}
