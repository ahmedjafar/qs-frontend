"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var question_service_1 = require("./question.service");
var status_reporting_service_1 = require("../status-reporting.service");
var question_1 = require("./question");
var QuestionEditorComponent = (function () {
    function QuestionEditorComponent(_questionService, _statusReportingService, _router, _activatedRoute) {
        this._questionService = _questionService;
        this._statusReportingService = _statusReportingService;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this.nextButtonEnableStatus = false;
        this.backButtonEnableStatus = false;
        this.deleteButtonEnableStatus = false;
        this.cancelButtonEnableStatus = false;
        this.isLocked = false;
        this.skipSave = false;
        this.token = localStorage.getItem('token');
        this.question = new question_1.Question();
        this.ids = new Array();
        _statusReportingService.removeStatus();
    }
    QuestionEditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.quizId = this._activatedRoute.snapshot.params['quiz_Id'];
        this._questionService.getQuestionIds(this.quizId, this.token)
            .subscribe(function (ids) { return _this.initializeQuestionEditor(ids); }, function (error) {
            if (error.status === 404) {
                _this._statusReportingService
                    .raiseError('Invalid quiz id!');
                _this.currentQuestionIndex = -1;
                _this.lockForm();
            }
            else {
                _this._statusReportingService
                    .raiseError('An error occurred while contacting server!');
            }
        });
    };
    QuestionEditorComponent.prototype.initializeQuestionEditor = function (ids) {
        this.ids = ids;
        if (this.ids.length > 0) {
            this.currentQuestionIndex = this.ids.length - 1;
            this.getQuestion(this.ids[this.currentQuestionIndex].id);
        }
        else {
            this.currentQuestionIndex = -1;
        }
        this._statusReportingService.removeStatus();
        this.nextButtonEnableStatus = true;
        if (this.ids.length > 1) {
            this.backButtonEnableStatus = true;
        }
    };
    QuestionEditorComponent.prototype.getQuestion = function (id) {
        var _this = this;
        this._questionService.getQuestion(id, this.token)
            .subscribe(function (question) {
            _this.question = question;
            _this.deleteButtonEnableStatus = true;
        }, function (error) {
            _this._statusReportingService.raiseError('An error occurred while fetching data, please refresh the page');
            _this.lockForm();
        });
    };
    QuestionEditorComponent.prototype.next = function () {
        this._statusReportingService.removeStatus();
        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        if (this.currentQuestionIndex < -1) {
            this.lockForm();
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        if (this.currentQuestionIndex === -1 || this.currentQuestionIndex > this.ids.length - 1) {
            // new question
            this.deleteButtonEnableStatus = false;
            this.createQuestion();
        }
        else {
            // existing question
            this.deleteButtonEnableStatus = true;
            this.updateQuestion(this.ids[this.currentQuestionIndex].id, true);
        }
    };
    QuestionEditorComponent.prototype.back = function () {
        this._statusReportingService.removeStatus();
        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        if (this.currentQuestionIndex < 0 || this.currentQuestionIndex > this.ids.length) {
            this.lockForm();
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        if (this.currentQuestionIndex >= 0 && this.currentQuestionIndex <= this.ids.length - 1) {
            this.updateQuestion(this.ids[this.currentQuestionIndex].id, false);
        }
    };
    QuestionEditorComponent.prototype.cancel = function () {
        if (this.currentQuestionIndex !== -1) {
            if (this.currentQuestionIndex === this.ids.length) {
                this.currentQuestionIndex--;
            }
            this.getQuestion(this.ids[this.currentQuestionIndex].id);
        }
    };
    QuestionEditorComponent.prototype.backToQuizEditor = function () {
        this._router.navigateByUrl("/admin/quizEditor/" + this.quizId);
    };
    QuestionEditorComponent.prototype.createQuestion = function () {
        var _this = this;
        var questionId = -1;
        this.question.Quiz_Id = this.quizId;
        if (!this.checkSelectedAnswersValidity()) {
            this._statusReportingService.raiseError('Please select at least one correct answer!');
            return;
        }
        else if (!this.checkOptionalChoicesOrder()) {
            this._statusReportingService.raiseError('Cannot leave choice 3 empty!');
            return;
        }
        this._questionService.createQuestion(this.question, this.token)
            .subscribe(function (response) {
            questionId = response.Id;
            _this.ids.push({ id: questionId });
            _this.currentQuestionIndex++;
            _this.question = new question_1.Question();
            _this._statusReportingService.reportSuccess('Save Succeeded');
        }, function (error) {
            _this._statusReportingService.raiseError('An error has occurred while saving data!');
        });
    };
    QuestionEditorComponent.prototype.updateQuestion = function (id, increment) {
        var _this = this;
        if (!this.checkSelectedAnswersValidity()) {
            this._statusReportingService.raiseError('Please select at least one correct answer!');
            return;
        }
        else if (!this.checkOptionalChoicesOrder()) {
            this._statusReportingService.raiseError('Cannot leave choice 3 empty!');
            return;
        }
        this._questionService.updateQuestion(id, this.question, this.token)
            .subscribe(function (response) {
            if (increment) {
                _this.currentQuestionIndex++;
            }
            else {
                _this.currentQuestionIndex--;
                if (_this.currentQuestionIndex === 0) {
                    _this.backButtonEnableStatus = false;
                }
            }
            if (_this.currentQuestionIndex >= 0 && _this.currentQuestionIndex <= _this.ids.length - 1) {
                _this.getQuestion(_this.ids[_this.currentQuestionIndex].id);
            }
            _this._statusReportingService.reportSuccess('Update Succeeded');
        }, function (error) {
            _this._statusReportingService.raiseError('An error has occurred while saving data!');
        });
    };
    QuestionEditorComponent.prototype.deleteQuestion = function () {
        var _this = this;
        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        this._statusReportingService.removeStatus();
        this._questionService.deleteQuestion(this.ids[this.currentQuestionIndex].id, this.token)
            .subscribe(function (response) {
            if (_this.currentQuestionIndex >= 0) {
                _this.ids.splice(_this.currentQuestionIndex, 1);
            }
            if (_this.currentQuestionIndex > 0) {
                if (_this.currentQuestionIndex === _this.ids.length) {
                    _this.currentQuestionIndex--;
                }
            }
            else if (_this.currentQuestionIndex === 0) {
                _this.backButtonEnableStatus = false;
                if (_this.ids.length === 0) {
                    _this.deleteButtonEnableStatus = false;
                    _this.currentQuestionIndex--;
                }
            }
            else {
                _this.backButtonEnableStatus = false;
                _this.deleteButtonEnableStatus = false;
            }
            if (_this.currentQuestionIndex >= 0) {
                _this.getQuestion(_this.ids[_this.currentQuestionIndex].id);
            }
        }, function (error) {
            _this._statusReportingService.raiseError('Delete failed!');
            _this.getQuestion(_this.ids[_this.currentQuestionIndex].id);
        });
    };
    QuestionEditorComponent.prototype.checkSelectedAnswersValidity = function () {
        if ((this.question.IsChoice1Correct == null || this.question.IsChoice1Correct === false) &&
            (this.question.IsChoice2Correct == null || this.question.IsChoice2Correct === false) &&
            (this.question.IsChoice3Correct == null || this.question.IsChoice3Correct === false) &&
            (this.question.IsChoice4Correct == null || this.question.IsChoice4Correct === false)) {
            return false;
        }
        else {
            return true;
        }
    };
    QuestionEditorComponent.prototype.checkOptionalChoicesOrder = function () {
        if (this.question.Choice3 == null && this.question.Choice4 != null) {
            return false;
        }
        else {
            return true;
        }
    };
    QuestionEditorComponent.prototype.lockForm = function () {
        this.isLocked = true;
    };
    return QuestionEditorComponent;
}());
QuestionEditorComponent = __decorate([
    core_1.Component({
        selector: 'question-editor',
        templateUrl: 'app/question/questionEditor.component.html',
    }),
    __metadata("design:paramtypes", [question_service_1.QuestionService, status_reporting_service_1.StatusReportingService,
        router_1.Router, router_1.ActivatedRoute])
], QuestionEditorComponent);
exports.QuestionEditorComponent = QuestionEditorComponent;
//# sourceMappingURL=questionEditor.component.js.map