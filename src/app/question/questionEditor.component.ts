import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionService } from './question.service';
import { StatusReportingService } from '../status-reporting.service';
import { Id } from '../Id';
import { Question } from './question';

@Component({
    selector: 'question-editor',
    templateUrl: 'app/question/questionEditor.component.html',
})
export class QuestionEditorComponent implements OnInit {
    token: string;
    quizId: number;
    nextButtonEnableStatus: boolean = false;
    backButtonEnableStatus: boolean = false;
    deleteButtonEnableStatus: boolean = false;
    cancelButtonEnableStatus: boolean = false;
    question: Question;
    currentQuestionIndex: number;
    ids: Id[];
    isLocked: boolean = false;
    skipSave: boolean = false;

    constructor(private _questionService: QuestionService, private _statusReportingService: StatusReportingService,
        private _router: Router, private _activatedRoute: ActivatedRoute) {
        this.token = localStorage.getItem('token');

        this.question = new Question();
        this.ids = new Array();

        _statusReportingService.removeStatus();
    }

    ngOnInit() {
        this.quizId = this._activatedRoute.snapshot.params['quiz_Id'];
        this._questionService.getQuestionIds(this.quizId, this.token)
            .subscribe((ids) => this.initializeQuestionEditor(ids),
            (error) => {
                if (error.status === 404) {
                    this._statusReportingService
                        .raiseError('Invalid quiz id!');
                    this.currentQuestionIndex = -1;
                    this.lockForm();
                } else {
                    this._statusReportingService
                        .raiseError('An error occurred while contacting server!');
                }
            });
    }

    initializeQuestionEditor(ids: Id[]) {
        this.ids = ids;
        if (this.ids.length > 0) {
            this.currentQuestionIndex = this.ids.length - 1;
            this.getQuestion(this.ids[this.currentQuestionIndex].id);
        } else { this.currentQuestionIndex = -1; }
        this._statusReportingService.removeStatus();
        this.nextButtonEnableStatus = true;
        if (this.ids.length > 1) {
            this.backButtonEnableStatus = true;
        }
    }

    getQuestion(id: number) {
        this._questionService.getQuestion(id, this.token)
            .subscribe((question) => {
                this.question = question;
                this.deleteButtonEnableStatus = true;
            },
            (error) => {
                this._statusReportingService.raiseError('An error occurred while fetching data, please refresh the page');
                this.lockForm();
            });
    }

    next(): void {
        this._statusReportingService.removeStatus();

        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        if (this.currentQuestionIndex < -1) {
            this.lockForm();
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        if (this.currentQuestionIndex === -1 || this.currentQuestionIndex > this.ids.length - 1) {
            // new question
            this.deleteButtonEnableStatus = false;
            this.createQuestion();
        } else {
            // existing question
            this.deleteButtonEnableStatus = true;
            this.updateQuestion(this.ids[this.currentQuestionIndex].id, true);
        }
    }

    back(): void {
        this._statusReportingService.removeStatus();

        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        if (this.currentQuestionIndex < 0 || this.currentQuestionIndex > this.ids.length) {
            this.lockForm();
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        if (this.currentQuestionIndex >= 0 && this.currentQuestionIndex <= this.ids.length - 1) {
            this.updateQuestion(this.ids[this.currentQuestionIndex].id, false);
        }
    }

    cancel(): void {
        if (this.currentQuestionIndex !== -1) {
            if (this.currentQuestionIndex === this.ids.length) {
                this.currentQuestionIndex--;
            }
            this.getQuestion(this.ids[this.currentQuestionIndex].id);
        }
    }

    backToQuizEditor(): void {
        this._router.navigateByUrl(`/admin/quizEditor/${this.quizId}`);
    }

    createQuestion() {
        let questionId: number = -1;
        this.question.Quiz_Id = this.quizId;

        if (!this.checkSelectedAnswersValidity()) {
            this._statusReportingService.raiseError('Please select at least one correct answer!');
            return;
        } else if (!this.checkOptionalChoicesOrder()) {
            this._statusReportingService.raiseError('Cannot leave choice 3 empty!');
            return;
        }

        this._questionService.createQuestion(this.question, this.token)
            .subscribe(
            (response) => {
                questionId = response.Id;
                this.ids.push({ id: questionId });
                this.currentQuestionIndex++;
                this.question = new Question();
                this._statusReportingService.reportSuccess('Save Succeeded');
            },
            (error) => {
                this._statusReportingService.raiseError('An error has occurred while saving data!');
            });
    }

    updateQuestion(id: number, increment: boolean) {
        if (!this.checkSelectedAnswersValidity()) {
            this._statusReportingService.raiseError('Please select at least one correct answer!');
            return;
        } else if (!this.checkOptionalChoicesOrder()) {
            this._statusReportingService.raiseError('Cannot leave choice 3 empty!');
            return;
        }

        this._questionService.updateQuestion(id, this.question, this.token)
            .subscribe(
            (response) => {
                if (increment) {
                    this.currentQuestionIndex++;
                } else {
                    this.currentQuestionIndex--;
                    if (this.currentQuestionIndex === 0) {
                        this.backButtonEnableStatus = false;
                    }
                }

                if (this.currentQuestionIndex >= 0 && this.currentQuestionIndex <= this.ids.length - 1) {
                    this.getQuestion(this.ids[this.currentQuestionIndex].id);
                }
                this._statusReportingService.reportSuccess('Update Succeeded');
            },
            (error) => {
                this._statusReportingService.raiseError('An error has occurred while saving data!');
            });
    }

    deleteQuestion() {
        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        this._statusReportingService.removeStatus();
        this._questionService.deleteQuestion(this.ids[this.currentQuestionIndex].id, this.token)
            .subscribe((response) => {
                if (this.currentQuestionIndex >= 0) {
                    this.ids.splice(this.currentQuestionIndex, 1);
                }

                if (this.currentQuestionIndex > 0) {
                    if (this.currentQuestionIndex === this.ids.length) {
                        this.currentQuestionIndex--;
                    }
                } else if (this.currentQuestionIndex === 0) {
                    this.backButtonEnableStatus = false;
                    if (this.ids.length === 0) {
                        this.deleteButtonEnableStatus = false;
                        this.currentQuestionIndex--;
                    }
                } else {
                    this.backButtonEnableStatus = false;
                    this.deleteButtonEnableStatus = false;
                }

                if (this.currentQuestionIndex >= 0) {
                    this.getQuestion(this.ids[this.currentQuestionIndex].id);
                }
            },
            (error) => {
                this._statusReportingService.raiseError('Delete failed!');
                this.getQuestion(this.ids[this.currentQuestionIndex].id);
            });
    }

    checkSelectedAnswersValidity(): boolean {
        if ((this.question.IsChoice1Correct == null || this.question.IsChoice1Correct === false) &&
            (this.question.IsChoice2Correct == null || this.question.IsChoice2Correct === false) &&
            (this.question.IsChoice3Correct == null || this.question.IsChoice3Correct === false) &&
            (this.question.IsChoice4Correct == null || this.question.IsChoice4Correct === false)) {
            return false;
        } else { return true; }
    }

    checkOptionalChoicesOrder(): boolean {
        if (this.question.Choice3 == null && this.question.Choice4 != null) {
            return false;
        } else {
            return true;
        }
    }

    lockForm() {
        this.isLocked = true;
    }
}
