import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { StatusReportingService } from '../status-reporting.service';

@Component({
    selector: 'all-assigned-quizzes',
    template: '<h1>Assigned Quizzes</h1>'
})
export class AllAssignedQuizzesComponent {
    token: string;

    constructor(private _statusReportingService: StatusReportingService, private _router: Router) {
    this.token = localStorage.getItem('token');
    if (this.token === null) {
        _router.navigateByUrl('/login');
    }
    _statusReportingService.removeStatus();
  }
}
