"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var quiz_service_1 = require("./quiz.service");
var status_reporting_service_1 = require("../status-reporting.service");
var AssignedQuizzesComponent = (function () {
    function AssignedQuizzesComponent(_quizService, _statusReportingService, _router) {
        this._quizService = _quizService;
        this._statusReportingService = _statusReportingService;
        this._router = _router;
        this.statusMessage = 'Loading data ...';
        this.token = localStorage.getItem('token');
        _statusReportingService.removeStatus();
    }
    AssignedQuizzesComponent.prototype.ngOnInit = function () {
        this.getQuizzes();
    };
    AssignedQuizzesComponent.prototype.trackByQuizId = function (index, quiz) {
        return quiz.Id;
    };
    AssignedQuizzesComponent.prototype.getQuizzes = function () {
        var _this = this;
        this.statusMessage = 'Loading data ...';
        this._quizService.getAvailableQuizzes(this.token)
            .subscribe(function (quizzes) { return _this.quizzes = quizzes; }, function (error) { return _this.statusMessage = 'An error has occurred during fetching data!'; });
    };
    return AssignedQuizzesComponent;
}());
AssignedQuizzesComponent = __decorate([
    core_1.Component({
        selector: 'assigned-quizzes',
        templateUrl: './assignedQuizzes.component.html'
    }),
    __metadata("design:paramtypes", [quiz_service_1.QuizService, status_reporting_service_1.StatusReportingService, router_1.Router])
], AssignedQuizzesComponent);
exports.AssignedQuizzesComponent = AssignedQuizzesComponent;
//# sourceMappingURL=assignedQuizzes.component.js.map