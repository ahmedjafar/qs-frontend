import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { QuizService } from './quiz.service';
import { StatusReportingService } from '../status-reporting.service';
import { IQuiz } from './quiz';

@Component({
  selector: 'assigned-quizzes',
  templateUrl: './assignedQuizzes.component.html'
})
export class AssignedQuizzesComponent implements OnInit {
  token: string;
  statusMessage: string = 'Loading data ...';
  quizzes: IQuiz[];

  constructor(private _quizService: QuizService, private _statusReportingService: StatusReportingService, private _router: Router) {
      this.token = localStorage.getItem('token');

      _statusReportingService.removeStatus();
   }

  ngOnInit() {
      this.getQuizzes();
  }

  trackByQuizId (index: number, quiz: IQuiz): number {
      return quiz.Id;
  }

  getQuizzes(): void {
      this.statusMessage = 'Loading data ...';
      this._quizService.getAvailableQuizzes(this.token)
                       .subscribe((quizzes) => this.quizzes = quizzes,
                                  (error) => this.statusMessage = 'An error has occurred during fetching data!');

  }
}
