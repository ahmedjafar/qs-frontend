"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_2 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/Observable/throw");
var QuizService = (function () {
    function QuizService(_http) {
        this._http = _http;
    }
    QuizService.prototype.getQuiz = function (id, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get("http://localhost:9768/api/quizzes/" + id, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuizService.prototype.getQuizzes = function (token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get("http://localhost:9768/api/quizzes", options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuizService.prototype.getAvailableQuizzes = function (token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.get("http://localhost:9768/api/AvailableQuizzes", options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuizService.prototype.createQuiz = function (quiz, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.post("http://localhost:9768/api/quizzes", quiz, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuizService.prototype.updateQuiz = function (id, quiz, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.put("http://localhost:9768/api/quizzes/" + id, quiz, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuizService.prototype.deleteQuiz = function (id, token) {
        var headers = new http_2.Headers();
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.delete("http://localhost:9768/api/quizzes/" + id, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    QuizService.prototype.handleError = function (error) {
        console.error(error);
        return Observable_1.Observable.throw(error);
    };
    return QuizService;
}());
QuizService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_2.Http])
], QuizService);
exports.QuizService = QuizService;
//# sourceMappingURL=quiz.service.js.map