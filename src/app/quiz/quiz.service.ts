import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { IQuiz } from './quiz';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';

@Injectable()
export class QuizService {
    constructor(private _http: Http) { }

    getQuiz(id: number, token: string): Observable<IQuiz> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/quizzes/${id}`, options)
        .map((response: Response) => <IQuiz>response.json())
        .catch(this.handleError);
    }

    getQuizzes(token: string): Observable<IQuiz[]> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/quizzes`, options)
        .map((response: Response) => <IQuiz[]>response.json())
        .catch(this.handleError);
    }

    getAvailableQuizzes(token: string): Observable<IQuiz[]> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/AvailableQuizzes`, options)
        .map((response: Response) => <IQuiz[]>response.json())
        .catch(this.handleError);
    }

    createQuiz(quiz: IQuiz, token: string): Observable<IQuiz> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.post(`http://localhost:9768/api/quizzes`, quiz, options)
        .map((response) => <IQuiz>response.json())
        .catch(this.handleError);
    }

    updateQuiz(id: number, quiz: IQuiz, token: string): Observable<IQuiz> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.put(`http://localhost:9768/api/quizzes/${id}`, quiz, options)
        .map((response) => <IQuiz>response.json())
        .catch(this.handleError);
    }

    deleteQuiz(id: number, token: string): Observable<Response> {
        let headers = new Headers();
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.delete(`http://localhost:9768/api/quizzes/${id}`, options)
        .map((response) => response.json())
        .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}
