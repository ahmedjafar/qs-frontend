export interface IQuiz {
    Id: number;
    Name: string;
    CreationDate: Date;
    ValidationDate: Date;
    NumberOfQuestions: number;
    Time: number;
}

export class Quiz implements IQuiz {
    Id: number;
    Name: string;
    CreationDate: Date;
    ValidationDate: Date;
    NumberOfQuestions: number;
    Time: number;

    constructor() {
     }
}
