"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var quiz_service_1 = require("./quiz.service");
var status_reporting_service_1 = require("../status-reporting.service");
var quiz_1 = require("./quiz");
var QuizCreatorComponent = (function () {
    function QuizCreatorComponent(_quizService, _statusReportingService, _router) {
        this._quizService = _quizService;
        this._statusReportingService = _statusReportingService;
        this._router = _router;
        this.addQuezzesButtonEnableStatus = false;
        this.statusMessage = 'Loading data ...';
        this.quiz = new quiz_1.Quiz();
        this.token = localStorage.getItem('token');
        _statusReportingService.removeStatus();
    }
    QuizCreatorComponent.prototype.listQuizzes = function () {
        this._router.navigateByUrl('admin/quizzesList');
    };
    QuizCreatorComponent.prototype.save = function () {
        var _this = this;
        if (this.addQuezzesButtonEnableStatus) {
            this.statusMessage = 'Data already saved!';
            this._statusReportingService.raiseError(this.statusMessage);
            return;
        }
        this.quiz.CreationDate = new Date(Date.now());
        this._quizService.createQuiz(this.quiz, this.token)
            .subscribe(function (response) {
            _this.statusMessage = 'Save Succeeded';
            _this.addQuezzesButtonEnableStatus = true;
            _this.quizId = response.Id.toString();
            _this.quiz = response;
            _this._statusReportingService.reportSuccess(_this.statusMessage);
        }, function (error) {
            _this.statusMessage = 'An error has occurred during saving data!';
            _this._statusReportingService.raiseError(_this.statusMessage);
        });
    };
    QuizCreatorComponent.prototype.addQuestions = function () {
        this._router.navigateByUrl("admin/questionEditor/" + this.quizId);
    };
    return QuizCreatorComponent;
}());
QuizCreatorComponent = __decorate([
    core_1.Component({
        selector: 'quiz-creator',
        templateUrl: 'app/quiz/quizCreator.component.html',
    }),
    __metadata("design:paramtypes", [quiz_service_1.QuizService, status_reporting_service_1.StatusReportingService, router_1.Router])
], QuizCreatorComponent);
exports.QuizCreatorComponent = QuizCreatorComponent;
//# sourceMappingURL=quizCreator.component.js.map