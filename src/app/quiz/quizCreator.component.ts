import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { QuizService } from './quiz.service';
import { StatusReportingService } from '../status-reporting.service';
import { Quiz } from './quiz';

@Component({
    selector: 'quiz-creator',
    templateUrl: 'app/quiz/quizCreator.component.html',
})
export class QuizCreatorComponent {
    token: string;
    quizId: string;
    addQuezzesButtonEnableStatus: boolean = false;
    statusMessage: string = 'Loading data ...';
    quiz: Quiz = new Quiz();

    constructor(private _quizService: QuizService, private _statusReportingService: StatusReportingService, private _router: Router) {
        this.token = localStorage.getItem('token');

        _statusReportingService.removeStatus();
     }

    listQuizzes(): void {
        this._router.navigateByUrl('admin/quizzesList');
    }

    save(): void {
        if (this.addQuezzesButtonEnableStatus) {
            this.statusMessage = 'Data already saved!';
            this._statusReportingService.raiseError(this.statusMessage);
            return;
        }
        this.quiz.CreationDate = new Date(Date.now());
        this._quizService.createQuiz(this.quiz, this.token)
                         .subscribe(
                                    (response) => { this.statusMessage = 'Save Succeeded';
                                                    this.addQuezzesButtonEnableStatus = true;
                                                    this.quizId = response.Id.toString();
                                                    this.quiz = <Quiz>response;
                                                    this._statusReportingService.reportSuccess(this.statusMessage); },
                                    (error) => { this.statusMessage = 'An error has occurred during saving data!';
                                                 this._statusReportingService.raiseError(this.statusMessage); }
                                    );
    }

    addQuestions(): void {
        this._router.navigateByUrl(`admin/questionEditor/${this.quizId}`);
    }
}
