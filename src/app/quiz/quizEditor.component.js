"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var quiz_service_1 = require("./quiz.service");
var status_reporting_service_1 = require("../status-reporting.service");
var quiz_1 = require("./quiz");
var QuizEditorComponent = (function () {
    function QuizEditorComponent(_quizService, _statusReportingService, _router, _activatedRoute) {
        this._quizService = _quizService;
        this._statusReportingService = _statusReportingService;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this.editQuezzesButtonEnableStatus = false;
        this.statusMessage = 'Loading data ...';
        this.quiz = new quiz_1.Quiz();
        this.isLocked = false;
        this.token = localStorage.getItem('token');
        _statusReportingService.removeStatus();
    }
    QuizEditorComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.quizId = this._activatedRoute.snapshot.params['id'];
        this._quizService.getQuiz(this.quizId, this.token).subscribe(function (quiz) { _this.quiz = quiz; }, function (error) {
            if (error.status === 404) {
                _this._statusReportingService
                    .raiseError(_this.statusMessage = 'Invalid quiz id!');
                _this.lockForm();
            }
            else {
                _this._statusReportingService
                    .raiseError(_this.statusMessage = 'An error occurred while contacting server!');
            }
        });
    };
    QuizEditorComponent.prototype.listQuizzes = function () {
        this._router.navigateByUrl('admin/quizzesList');
    };
    QuizEditorComponent.prototype.save = function () {
        var _this = this;
        if (this.editQuezzesButtonEnableStatus) {
            this.statusMessage = 'Data already saved!';
            this._statusReportingService.raiseError(this.statusMessage);
            return;
        }
        this.quiz.CreationDate = new Date(Date.now());
        this._quizService.updateQuiz(this.quizId, this.quiz, this.token)
            .subscribe(function (response) {
            _this.statusMessage = 'Save Succeeded';
            _this._statusReportingService.reportSuccess(_this.statusMessage);
        }, function (error) {
            _this.statusMessage = 'An error has occurred during saving data!';
            _this._statusReportingService.raiseError(_this.statusMessage);
        });
    };
    QuizEditorComponent.prototype.editQuestions = function () {
        this._router.navigateByUrl("admin/questionEditor/" + this.quizId);
    };
    QuizEditorComponent.prototype.lockForm = function () {
        this.isLocked = true;
    };
    return QuizEditorComponent;
}());
QuizEditorComponent = __decorate([
    core_1.Component({
        selector: 'quiz-editor',
        templateUrl: 'app/quiz/quizEditor.component.html'
    }),
    __metadata("design:paramtypes", [quiz_service_1.QuizService, status_reporting_service_1.StatusReportingService,
        router_1.Router, router_1.ActivatedRoute])
], QuizEditorComponent);
exports.QuizEditorComponent = QuizEditorComponent;
//# sourceMappingURL=quizEditor.component.js.map