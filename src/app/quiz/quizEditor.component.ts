import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { QuizService } from './quiz.service';
import { StatusReportingService } from '../status-reporting.service';
import { Quiz } from './quiz';

@Component({
    selector: 'quiz-editor',
    templateUrl: 'app/quiz/quizEditor.component.html'
})
export class QuizEditorComponent implements OnInit {
    token: string;
    quizId: number;
    editQuezzesButtonEnableStatus: boolean = false;
    statusMessage: string = 'Loading data ...';
    quiz: Quiz = new Quiz();
    isLocked: boolean = false;

    constructor(private _quizService: QuizService, private _statusReportingService: StatusReportingService,
        private _router: Router, private _activatedRoute: ActivatedRoute) {
        this.token = localStorage.getItem('token');

        _statusReportingService.removeStatus();
     }

     ngOnInit() {
        this.quizId = this._activatedRoute.snapshot.params['id'];
        this._quizService.getQuiz(this.quizId, this.token).subscribe(
            (quiz) => { this.quiz = quiz; },
            (error) =>  {
                if (error.status === 404) {
                    this._statusReportingService
                    .raiseError(this.statusMessage = 'Invalid quiz id!');
                    this.lockForm();
                } else {
                this._statusReportingService
                    .raiseError(this.statusMessage = 'An error occurred while contacting server!');
                }
        });
     }

    listQuizzes(): void {
        this._router.navigateByUrl('admin/quizzesList');
    }

    save(): void {
        if (this.editQuezzesButtonEnableStatus) {
            this.statusMessage = 'Data already saved!';
            this._statusReportingService.raiseError(this.statusMessage);
            return;
        }
        this.quiz.CreationDate = new Date(Date.now());
        this._quizService.updateQuiz(this.quizId, this.quiz, this.token)
                         .subscribe(
                                    (response) => { this.statusMessage = 'Save Succeeded';
                                                    this._statusReportingService.reportSuccess(this.statusMessage); },
                                    (error) => { this.statusMessage = 'An error has occurred during saving data!';
                                                 this._statusReportingService.raiseError(this.statusMessage); }
                                    );
    }

    editQuestions(): void {
        this._router.navigateByUrl(`admin/questionEditor/${this.quizId}`);
    }

    lockForm() {
        this.isLocked = true;
    }
 }
