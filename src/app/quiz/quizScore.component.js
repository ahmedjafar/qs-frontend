"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var QuizScoreComponent = (function () {
    function QuizScoreComponent(_router) {
        this._router = _router;
        this.score = null;
    }
    QuizScoreComponent.prototype.ngOnInit = function () {
        this.score = sessionStorage.getItem('score');
    };
    QuizScoreComponent.prototype.refresh = function () {
        this._router.navigateByUrl('testee/quizScore');
    };
    QuizScoreComponent.prototype.backToAssignedQuizzes = function () {
        //sessionStorage.removeItem('score');
        this._router.navigateByUrl('testee/assignedQuizzes');
    };
    return QuizScoreComponent;
}());
QuizScoreComponent = __decorate([
    core_1.Component({
        selector: 'quiz-score',
        templateUrl: 'app/quiz/quizScore.component.html',
    }),
    __metadata("design:paramtypes", [router_1.Router])
], QuizScoreComponent);
exports.QuizScoreComponent = QuizScoreComponent;
//# sourceMappingURL=quizScore.component.js.map