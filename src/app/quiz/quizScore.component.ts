import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
    selector: 'quiz-score',
    templateUrl: 'app/quiz/quizScore.component.html',
})
export class QuizScoreComponent implements OnInit {
    token: string;
    score: string = null;

    constructor(private _router: Router) {  }

    ngOnInit() {
        this.score = sessionStorage.getItem('score');
    }

    refresh() {
        this._router.navigateByUrl('testee/quizScore');
    }

     backToAssignedQuizzes() {
         //sessionStorage.removeItem('score');
        this._router.navigateByUrl('testee/assignedQuizzes');
     }
}
