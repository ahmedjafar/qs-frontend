import { Injectable } from '@angular/core';
import { RequestOptions } from '@angular/http';
import { IQuizScore } from './quizScore';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';

@Injectable()
export class QuizScoreService {
    constructor(private _http: Http) { }

    getQuizScores(userId: string, token: string): Observable<IQuizScore[]> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/QuizScores?userId=${userId}` , options)
                         .map((response) => <IQuizScore[]>response.json())
                         .catch(this.handleError);
    }

    getQuizScore(id: number, token: string): Observable<IQuizScore> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.get(`http://localhost:9768/api/QuizScores/${id}`, options)
                         .map((response) => <IQuizScore>response.json())
                         .catch(this.handleError);
    }

    initiateQuizScore(quizScore: IQuizScore, token: string): Observable<IQuizScore> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.post(`http://localhost:9768/api/QuizScores/`, quizScore, options)
                         .map((response) => <IQuizScore>response.json())
                         .catch(this.handleError);
    }

    calculateQuizScore(id: number, quizScore: IQuizScore, token: string): Observable<IQuizScore> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        return this._http.put(`http://localhost:9768/api/QuizScores/${id}`, quizScore, options)
                         .map((response) => <IQuizScore>response.json())
                         .catch(this.handleError);
    }

    private handleError(error: Response) {
        console.error(error);
        return Observable.throw(error);
    }
}
