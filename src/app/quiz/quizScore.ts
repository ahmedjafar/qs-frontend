export interface IQuizScore {
    Id: number;
    Quiz_Id: number;
    User_Id: string;
    Score: number;
    Date: Date;
}

export class QuizScore implements IQuizScore {
    Id: number;
    Quiz_Id: number;
    User_Id: string;
    Score: number;
    Date: Date;

    constructor() {
     }
}
