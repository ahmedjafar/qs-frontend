"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var common_1 = require("@angular/common");
var router_1 = require("@angular/router");
var question_service_1 = require("../question/question.service");
var answer_service_1 = require("../answer/answer.service");
var quizScore_service_1 = require("../quiz/quizScore.service");
var status_reporting_service_1 = require("../status-reporting.service");
var question_1 = require("../question/question");
var answer_1 = require("../answer/answer");
var quizScore_1 = require("../quiz/quizScore");
var QuizViewerComponent = (function () {
    function QuizViewerComponent(_questionService, _answerService, _statusReportingService, _quizScoreService, _router, _activatedRoute, _location) {
        // disable browser back navigation
        /*
        _location.onPopState(() => {
            return false;
        });
        */
        this._questionService = _questionService;
        this._answerService = _answerService;
        this._statusReportingService = _statusReportingService;
        this._quizScoreService = _quizScoreService;
        this._router = _router;
        this._activatedRoute = _activatedRoute;
        this._location = _location;
        this.userId = localStorage.getItem('userId');
        this.nextButtonEnableStatus = false;
        this.backButtonEnableStatus = false;
        this.isLocked = false;
        this.question = new question_1.Question();
        this.answer = new answer_1.Answer();
        this.quizScore = new quizScore_1.QuizScore();
        this.ids = new Array();
        this.answerIds = new Array();
        this.savedAnswers = new Array();
        this.token = localStorage.getItem('token');
        _statusReportingService.removeStatus();
    }
    QuizViewerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.quizId = this._activatedRoute.snapshot.params['quiz_Id'];
        this._questionService.getQuestionIds(this.quizId, this.token)
            .subscribe(function (ids) {
            _this.ids = ids;
            _this._answerService.getAnswerIds(_this.userId, _this.quizId, _this.token)
                .subscribe(function (answerIds) {
                _this.answerIds = answerIds;
                _this.quizScore.Quiz_Id = _this.quizId;
                _this.quizScore.User_Id = _this.userId;
                _this.quizScore.Date = new Date(Date.now());
                _this._quizScoreService.initiateQuizScore(_this.quizScore, _this.token).subscribe(function (quizScore) {
                    _this.scoreId = quizScore.Id;
                    _this.initializeQuizViewer();
                    sessionStorage.setItem('quizStarted', 'true');
                }, function (error) {
                    if (error.status === 403) {
                        _this.currentQuestionIndex = 0;
                        _this._statusReportingService.raiseError('This quiz has expired!');
                        _this.lockForm();
                    }
                    else {
                        _this._statusReportingService.raiseError('An error occurred while fetching data, please refresh the page');
                    }
                });
            }, function (error) {
                if (error.status === 404) {
                    _this._statusReportingService
                        .raiseError('Invalid quiz id!');
                    _this.currentQuestionIndex = -1;
                    _this.lockForm();
                }
                else {
                    _this._statusReportingService
                        .raiseError('An error occurred while contacting server!');
                }
            });
        }, function (error) {
            if (error.status === 404) {
                _this._statusReportingService
                    .raiseError('Invalid quiz id!');
                _this.currentQuestionIndex = -1;
                _this.lockForm();
            }
            else {
                _this._statusReportingService
                    .raiseError('An error occurred while contacting server!');
            }
        });
    };
    QuizViewerComponent.prototype.initializeQuizViewer = function () {
        if (this.ids.length > 0) {
            this.currentQuestionIndex = 0;
            this.getQuestion(this.ids[this.currentQuestionIndex].id);
            if (this.answerIds.length > 0) {
                console.log(this.answerIds[0].Answer_Id);
                this.getAnswer(this.answerIds[this.currentQuestionIndex].Answer_Id);
            }
        }
        else {
            this.currentQuestionIndex = -1;
            this._statusReportingService.raiseError('Error trying to attempt an empty quiz!');
            this.lockForm();
            return;
        }
        this._statusReportingService.removeStatus();
        this.nextButtonEnableStatus = true;
    };
    QuizViewerComponent.prototype.next = function () {
        var _this = this;
        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        if (this.currentQuestionIndex > this.ids.length - 1) {
            this.lockForm();
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        this._statusReportingService.removeStatus();
        var currentAnswerId = this.answerIds.find(function (a) { return a.Question_Id === _this.ids[_this.currentQuestionIndex].id; });
        if (currentAnswerId === undefined) {
            // new answer
            this.postAnswer(true);
        }
        else {
            // existing answer
            this.updateAnswer(currentAnswerId.Answer_Id, true);
        }
    };
    QuizViewerComponent.prototype.back = function () {
        var _this = this;
        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        if (this.currentQuestionIndex < 0) {
            this.lockForm();
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }
        var currentAnswerId = this.answerIds.find(function (a) { return a.Question_Id === _this.ids[_this.currentQuestionIndex].id; });
        if (currentAnswerId === undefined) {
            // new answer
            this.postAnswer(false);
        }
        else {
            // existing answer
            this.updateAnswer(currentAnswerId.Answer_Id, false);
        }
    };
    QuizViewerComponent.prototype.endQuiz = function () {
        var _this = this;
        this.quizScore.Id = this.scoreId;
        this.quizScore.Quiz_Id = this.quizId;
        this.quizScore.User_Id = this.userId;
        this.quizScore.Date = new Date(Date.now());
        this._quizScoreService.calculateQuizScore(this.scoreId, this.quizScore, this.token).subscribe(function (quizScore) {
            _this.quizScore = quizScore;
            console.log(quizScore.Score);
            sessionStorage.setItem('score', quizScore.Score.toString());
            sessionStorage.removeItem('quizStarted');
            _this._router.navigateByUrl("/testee/quizScore");
        }, function (error) {
            _this._statusReportingService.raiseError('An error occurred while fetching data, please refresh the page');
        });
    };
    QuizViewerComponent.prototype.getQuestion = function (id) {
        var _this = this;
        this._questionService.getQuestionSecured(id, this.token)
            .subscribe(function (question) {
            _this.question = question;
        }, function (error) {
            _this._statusReportingService.raiseError('An error occurred while fetching data, please refresh the page');
            _this.lockForm();
        });
    };
    QuizViewerComponent.prototype.getAnswer = function (id) {
        var _this = this;
        this._answerService.getAnswer(id, this.token)
            .subscribe(function (answer) {
            _this.answer = answer;
        }, function (error) {
            _this._statusReportingService.raiseError('An error occurred while fetching data, please refresh the page');
            _this.lockForm();
        });
    };
    QuizViewerComponent.prototype.postAnswer = function (increment) {
        var _this = this;
        this.answer.Quiz_Id = this.quizId;
        this.answer.Question_Id = this.ids[this.currentQuestionIndex].id;
        this.answer.User_Id = this.userId;
        this._answerService.postAnswer(this.answer, this.token)
            .subscribe(function (answer) {
            _this.answerIds.push({ Question_Id: answer.Question_Id, Answer_Id: answer.Id });
            _this.handleIndexAndButtos(increment);
            _this.getQuestion(_this.ids[_this.currentQuestionIndex].id);
            var currentAnswerId = _this.answerIds.find(function (a) { return a.Question_Id === _this.ids[_this.currentQuestionIndex].id; });
            if (currentAnswerId !== undefined) {
                _this.getAnswer(currentAnswerId.Answer_Id);
            }
            _this._statusReportingService.reportSuccess('Save Succeeded');
        }, function (error) {
            if (error.status === 403) {
                _this._statusReportingService.raiseError('Your time has expired!');
                _this.endQuiz();
            }
            else {
                _this._statusReportingService.raiseError('An error has occurred while saving data!');
            }
        });
    };
    QuizViewerComponent.prototype.updateAnswer = function (id, increment) {
        var _this = this;
        this.answer.Quiz_Id = this.quizId;
        this.answer.Question_Id = this.ids[this.currentQuestionIndex].id;
        this.answer.User_Id = this.userId;
        this._answerService.updateAnswer(id, this.answer, this.token)
            .subscribe(function (response) {
            _this.handleIndexAndButtos(increment);
            _this.getQuestion(_this.ids[_this.currentQuestionIndex].id);
            var currentAnswerId = _this.answerIds.find(function (a) { return a.Question_Id === _this.ids[_this.currentQuestionIndex].id; });
            if (currentAnswerId !== undefined) {
                _this.getAnswer(currentAnswerId.Answer_Id);
            }
            _this._statusReportingService.reportSuccess('Update Succeeded');
        }, function (error) {
            if (error.status === 403) {
                _this._statusReportingService.raiseError('Your time has expired!');
                _this.endQuiz();
            }
            else {
                _this._statusReportingService.raiseError('An error has occurred while saving data!');
            }
        });
    };
    QuizViewerComponent.prototype.handleIndexAndButtos = function (increment) {
        if (this.currentQuestionIndex === this.ids.length - 1) {
            if (increment) {
                this.nextButtonEnableStatus = false;
            }
            else {
                if (this.currentQuestionIndex > 0) {
                    this.currentQuestionIndex--;
                }
                else {
                    this.backButtonEnableStatus = false;
                    this.nextButtonEnableStatus = true;
                    return;
                }
                this.nextButtonEnableStatus = true;
            }
            this.backButtonEnableStatus = true;
        }
        else if (this.currentQuestionIndex === 0) {
            if (increment) {
                this.currentQuestionIndex++;
                this.backButtonEnableStatus = true;
            }
            else {
                this.backButtonEnableStatus = false;
            }
            this.nextButtonEnableStatus = true;
        }
        else {
            if (increment) {
                this.currentQuestionIndex++;
            }
            else {
                this.currentQuestionIndex--;
            }
            this.nextButtonEnableStatus = true;
            this.backButtonEnableStatus = true;
        }
    };
    QuizViewerComponent.prototype.lockForm = function () {
        this.isLocked = true;
    };
    return QuizViewerComponent;
}());
QuizViewerComponent = __decorate([
    core_1.Component({
        selector: 'quiz-viewer',
        templateUrl: 'app/quiz/quizViewer.component.html',
    }),
    __metadata("design:paramtypes", [question_service_1.QuestionService, answer_service_1.AnswerService,
        status_reporting_service_1.StatusReportingService, quizScore_service_1.QuizScoreService,
        router_1.Router, router_1.ActivatedRoute,
        common_1.PlatformLocation])
], QuizViewerComponent);
exports.QuizViewerComponent = QuizViewerComponent;
//# sourceMappingURL=quizViewer.component.js.map