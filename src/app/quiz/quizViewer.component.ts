import { Component, OnInit } from '@angular/core';
import { PlatformLocation } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { QuestionService } from '../question/question.service';
import { AnswerService } from '../answer/answer.service';
import { QuizScoreService } from '../quiz/quizScore.service';
import { StatusReportingService } from '../status-reporting.service';
import { Id, IAnswerId } from '../Id';
import { Question } from '../question/question';
import { Answer } from '../answer/answer';
import { QuizScore } from '../quiz/quizScore';
import { Response } from '@angular/http';

@Component({
    selector: 'quiz-viewer',
    templateUrl: 'app/quiz/quizViewer.component.html',
})
export class QuizViewerComponent implements OnInit {
    token: string;
    quizId: number;
    scoreId: number;
    userId: string = localStorage.getItem('userId');
    nextButtonEnableStatus: boolean = false;
    backButtonEnableStatus: boolean = false;
    question: Question;
    answer: Answer;
    quizScore: QuizScore;
    currentQuestionIndex: number;
    ids: Id[];
    answerIds: IAnswerId[];
    savedAnswers: Id[];
    isLocked: boolean = false;
    finalScore: number;

    constructor(private _questionService: QuestionService, private _answerService: AnswerService,
        private _statusReportingService: StatusReportingService, private _quizScoreService: QuizScoreService,
        private _router: Router, private _activatedRoute: ActivatedRoute,
        private _location: PlatformLocation) {

        // disable browser back navigation
        /*
        _location.onPopState(() => {
            return false;
        });
        */

        this.question = new Question();
        this.answer = new Answer();
        this.quizScore = new QuizScore();
        this.ids = new Array();
        this.answerIds = new Array();
        this.savedAnswers = new Array();
        this.token = localStorage.getItem('token');

        _statusReportingService.removeStatus();
    }

    ngOnInit() {
        this.quizId = this._activatedRoute.snapshot.params['quiz_Id'];
        this._questionService.getQuestionIds(this.quizId, this.token)
            .subscribe((ids) => {
                this.ids = ids;
                this._answerService.getAnswerIds(this.userId, this.quizId, this.token)
                    .subscribe(
                    (answerIds) => {
                        this.answerIds = answerIds;
                        this.quizScore.Quiz_Id = this.quizId;
                        this.quizScore.User_Id = this.userId;
                        this.quizScore.Date = new Date(Date.now());

                        this._quizScoreService.initiateQuizScore(this.quizScore, this.token).subscribe(
                            (quizScore) => {
                                 this.scoreId = quizScore.Id;
                                 this.initializeQuizViewer();
                                  sessionStorage.setItem('quizStarted', 'true');
                                 },
                            (error: Response) => {
                                if (error.status === 403) {
                                    this.currentQuestionIndex = 0;
                                    this._statusReportingService.raiseError('This quiz has expired!');
                                    this.lockForm();
                                } else {
                                this._statusReportingService.raiseError(
                                    'An error occurred while fetching data, please refresh the page');
                                }
                            });
                    },
                    (error) => {
                        if (error.status === 404) {
                            this._statusReportingService
                                .raiseError('Invalid quiz id!');
                            this.currentQuestionIndex = -1;
                            this.lockForm();
                        } else {
                            this._statusReportingService
                                .raiseError('An error occurred while contacting server!');
                        }
                    });
            },
            (error) => {
                if (error.status === 404) {
                    this._statusReportingService
                        .raiseError('Invalid quiz id!');
                    this.currentQuestionIndex = -1;
                    this.lockForm();
                } else {
                    this._statusReportingService
                        .raiseError('An error occurred while contacting server!');
                }
            });
    }

    initializeQuizViewer() {
        if (this.ids.length > 0) {
            this.currentQuestionIndex = 0;
            this.getQuestion(this.ids[this.currentQuestionIndex].id);
            if (this.answerIds.length > 0) {
                console.log(this.answerIds[0].Answer_Id);
                this.getAnswer(this.answerIds[this.currentQuestionIndex].Answer_Id);
            }
        } else {
            this.currentQuestionIndex = -1;
            this._statusReportingService.raiseError('Error trying to attempt an empty quiz!');
            this.lockForm();
            return;
        }
        this._statusReportingService.removeStatus();
        this.nextButtonEnableStatus = true;
    }

    next(): void {
        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        if (this.currentQuestionIndex > this.ids.length - 1) {
            this.lockForm();
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        this._statusReportingService.removeStatus();

        let currentAnswerId = this.answerIds.find(a => a.Question_Id === this.ids[this.currentQuestionIndex].id);
        if (currentAnswerId === undefined) {
            // new answer
            this.postAnswer(true);
        } else {
            // existing answer
            this.updateAnswer(currentAnswerId.Answer_Id, true);
        }
    }

    back(): void {
        if (this.isLocked) {
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        if (this.currentQuestionIndex < 0) {
            this.lockForm();
            this._statusReportingService.raiseError('Page is locked, please refresh the page to release lock');
            return;
        }

        let currentAnswerId = this.answerIds.find(a => a.Question_Id === this.ids[this.currentQuestionIndex].id);
        if (currentAnswerId === undefined) {
            // new answer
            this.postAnswer(false);
        } else {
            // existing answer
            this.updateAnswer(currentAnswerId.Answer_Id, false);
        }
    }

    endQuiz(): void {
        this.quizScore.Id = this.scoreId;
        this.quizScore.Quiz_Id = this.quizId;
        this.quizScore.User_Id = this.userId;
        this.quizScore.Date = new Date(Date.now());
        this._quizScoreService.calculateQuizScore(this.scoreId, this.quizScore, this.token).subscribe(
            (quizScore) => { this.quizScore = quizScore;
                console.log(quizScore.Score);
                sessionStorage.setItem('score', quizScore.Score.toString());
                sessionStorage.removeItem('quizStarted');
                this._router.navigateByUrl(`/testee/quizScore`);
             },
            (error) => {
                this._statusReportingService.raiseError(
                    'An error occurred while fetching data, please refresh the page');
            }
        );
    }

    getQuestion(id: number) {
        this._questionService.getQuestionSecured(id, this.token)
            .subscribe((question) => {
                this.question = question;
            },
            (error) => {
                this._statusReportingService.raiseError(
                    'An error occurred while fetching data, please refresh the page');
                this.lockForm();
            });
    }

    getAnswer(id: number) {
        this._answerService.getAnswer(id, this.token)
            .subscribe((answer) => {
                this.answer = answer;
            },
            (error) => {
                this._statusReportingService.raiseError(
                    'An error occurred while fetching data, please refresh the page');
                this.lockForm();
            });
    }

    postAnswer(increment: boolean) {
        this.answer.Quiz_Id = this.quizId;
        this.answer.Question_Id = this.ids[this.currentQuestionIndex].id;
        this.answer.User_Id = this.userId;

        this._answerService.postAnswer(this.answer, this.token)
            .subscribe(
            (answer) => {
                this.answerIds.push({ Question_Id: answer.Question_Id, Answer_Id: answer.Id });
                this.handleIndexAndButtos(increment);
                this.getQuestion(this.ids[this.currentQuestionIndex].id);
                let currentAnswerId = this.answerIds.find(a => a.Question_Id === this.ids[this.currentQuestionIndex].id);
                if (currentAnswerId !== undefined) {
                    this.getAnswer(currentAnswerId.Answer_Id);
                }
                this._statusReportingService.reportSuccess('Save Succeeded');
            },
            (error) => {
                if (error.status === 403) {
                    this._statusReportingService.raiseError('Your time has expired!');
                    this.endQuiz();
                } else {
                    this._statusReportingService.raiseError('An error has occurred while saving data!');
                }
            });
    }

    updateAnswer(id: number, increment: boolean) {
        this.answer.Quiz_Id = this.quizId;
        this.answer.Question_Id = this.ids[this.currentQuestionIndex].id;
        this.answer.User_Id = this.userId;

        this._answerService.updateAnswer(id, this.answer, this.token)
            .subscribe(
            (response) => {
                this.handleIndexAndButtos(increment);
                this.getQuestion(this.ids[this.currentQuestionIndex].id);
                let currentAnswerId = this.answerIds.find(a => a.Question_Id === this.ids[this.currentQuestionIndex].id);
                if (currentAnswerId !== undefined) {
                    this.getAnswer(currentAnswerId.Answer_Id);
                }

                this._statusReportingService.reportSuccess('Update Succeeded');
            },
            (error) => {
                if (error.status === 403) {
                    this._statusReportingService.raiseError('Your time has expired!');
                    this.endQuiz();
                } else {
                    this._statusReportingService.raiseError('An error has occurred while saving data!');
                }
            });
    }

    handleIndexAndButtos(increment: boolean) {
        if (this.currentQuestionIndex === this.ids.length - 1) {
            if (increment) {
                this.nextButtonEnableStatus = false;
            } else {
                if (this.currentQuestionIndex > 0) {
                    this.currentQuestionIndex--;
                } else {
                    this.backButtonEnableStatus = false;
                    this.nextButtonEnableStatus = true;
                    return;
                }
                this.nextButtonEnableStatus = true;
            }
            this.backButtonEnableStatus = true;
        } else if (this.currentQuestionIndex === 0) {
            if (increment) {
                this.currentQuestionIndex++;
                this.backButtonEnableStatus = true;
            } else {
                this.backButtonEnableStatus = false;
            }
            this.nextButtonEnableStatus = true;
        } else {
            if (increment) {
                this.currentQuestionIndex++;
            } else {
                this.currentQuestionIndex--;
            }
            this.nextButtonEnableStatus = true;
            this.backButtonEnableStatus = true;
        }
    }

    lockForm() {
        this.isLocked = true;
    }
}
