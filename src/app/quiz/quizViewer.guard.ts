import { Injectable } from '@angular/core';
import { CanDeactivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { QuizViewerComponent } from './quizViewer.component';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class QuizViewerGuard implements CanDeactivate<QuizViewerComponent> {
  private _quizStartedStorageKey: string = 'quizStarted';

  canDeactivate(
    component: QuizViewerComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    if (localStorage.getItem(this._quizStartedStorageKey)) {
      return false;
    }

    return true;
  }
}

