"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var quiz_service_1 = require("./quiz.service");
var status_reporting_service_1 = require("../status-reporting.service");
var QuizzesListComponent = (function () {
    function QuizzesListComponent(_quizService, _statusReportingService, _router) {
        this._quizService = _quizService;
        this._statusReportingService = _statusReportingService;
        this._router = _router;
        this.statusMessage = 'Loading data ...';
        this.errorRaised = new core_1.EventEmitter();
        this.token = localStorage.getItem('token');
        if (this.token === null) {
            _router.navigateByUrl('/login');
        }
        _statusReportingService.removeStatus();
    }
    QuizzesListComponent.prototype.ngOnInit = function () {
        this.getQuizzes();
    };
    QuizzesListComponent.prototype.trackByQuizId = function (index, quiz) {
        return quiz.Id;
    };
    QuizzesListComponent.prototype.createQuiz = function () {
        this._router.navigateByUrl('admin/quizCreator');
    };
    QuizzesListComponent.prototype.getQuizzes = function () {
        var _this = this;
        this.statusMessage = 'Loading data ...';
        this._quizService.getQuizzes(this.token)
            .subscribe(function (quizzes) { return _this.quizzes = quizzes; }, function (error) { return _this.statusMessage = 'An error has occurred during fetching data!'; });
    };
    QuizzesListComponent.prototype.deleteQuiz = function (id) {
        var _this = this;
        this._quizService.deleteQuiz(id, this.token)
            .subscribe(function (response) { _this.getQuizzes(); }, function (error) {
            _this.statusMessage = 'Delete failed!';
            _this._statusReportingService.raiseError(_this.statusMessage);
        });
    };
    QuizzesListComponent.prototype.onErrorRaised = function () {
        this.errorRaised.emit(this.statusMessage);
    };
    return QuizzesListComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], QuizzesListComponent.prototype, "errorRaised", void 0);
QuizzesListComponent = __decorate([
    core_1.Component({
        selector: 'quizzes-list',
        templateUrl: 'app/quiz/quizzesList.component.html',
    }),
    __metadata("design:paramtypes", [quiz_service_1.QuizService, status_reporting_service_1.StatusReportingService, router_1.Router])
], QuizzesListComponent);
exports.QuizzesListComponent = QuizzesListComponent;
//# sourceMappingURL=quizzesList.component.js.map