import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { QuizService } from './quiz.service';
import { StatusReportingService } from '../status-reporting.service';
import { IQuiz } from './quiz';

@Component({
    selector: 'quizzes-list',
    templateUrl: 'app/quiz/quizzesList.component.html',
})
export class QuizzesListComponent implements OnInit {
    token: string;
    statusMessage: string = 'Loading data ...';
    quizzes: IQuiz[];

    @Output()
    errorRaised: EventEmitter<string> = new EventEmitter<string>();

    constructor(private _quizService: QuizService, private _statusReportingService: StatusReportingService, private _router: Router) {
        this.token = localStorage.getItem('token');
        if (this.token === null) {
            _router.navigateByUrl('/login');
        }
        _statusReportingService.removeStatus();
     }

    ngOnInit() {
        this.getQuizzes();
    }

    trackByQuizId (index: number, quiz: IQuiz): number {
        return quiz.Id;
    }

    createQuiz(): void {
        this._router.navigateByUrl('admin/quizCreator');
    }

    getQuizzes(): void {
        this.statusMessage = 'Loading data ...';
        this._quizService.getQuizzes(this.token)
                         .subscribe((quizzes) => this.quizzes = quizzes,
                                    (error) => this.statusMessage = 'An error has occurred during fetching data!');

    }

    deleteQuiz(id: number): void {
        this._quizService.deleteQuiz(id, this.token)
        .subscribe((response) => { this.getQuizzes(); },
                   (error) => {
                       this.statusMessage = 'Delete failed!';
                       this._statusReportingService.raiseError(this.statusMessage);
                     });
    }

    onErrorRaised(): void {
        this.errorRaised.emit(this.statusMessage);
    }
 }

