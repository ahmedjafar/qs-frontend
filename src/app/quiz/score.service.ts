import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class ScoreService {
    // Observable string sources
    private scorePublishedSource = new Subject<string>();

    // Observable string streams
    errorPublished$ = this.scorePublishedSource.asObservable();

    // Service message commands
    publishScore(score: string) {
        this.scorePublishedSource.next(score);
      }
}
