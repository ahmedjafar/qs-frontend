"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var Subject_1 = require("rxjs/Subject");
var StatusReportingService = (function () {
    function StatusReportingService() {
        // Observable string sources
        this.errorRaisedSource = new Subject_1.Subject();
        this.successReportedSource = new Subject_1.Subject();
        this.removeStatusReportedSource = new Subject_1.Subject();
        // Observable string streams
        this.errorRaised$ = this.errorRaisedSource.asObservable();
        this.successReported$ = this.successReportedSource.asObservable();
        this.removeStatusReported$ = this.removeStatusReportedSource.asObservable();
    }
    // Service message commands
    StatusReportingService.prototype.raiseError = function (errorMessage) {
        this.errorRaisedSource.next(errorMessage);
    };
    StatusReportingService.prototype.reportSuccess = function (successMessage) {
        this.successReportedSource.next(successMessage);
    };
    StatusReportingService.prototype.removeStatus = function () {
        this.removeStatusReportedSource.next();
    };
    return StatusReportingService;
}());
StatusReportingService = __decorate([
    core_1.Injectable()
], StatusReportingService);
exports.StatusReportingService = StatusReportingService;
//# sourceMappingURL=status-reporting.service.js.map