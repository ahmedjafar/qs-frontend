import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class StatusReportingService {
    // Observable string sources
    private errorRaisedSource = new Subject<string>();
    private successReportedSource = new Subject<string>();
    private removeStatusReportedSource = new Subject<string>();

    // Observable string streams
    errorRaised$ = this.errorRaisedSource.asObservable();
    successReported$ = this.successReportedSource.asObservable();
    removeStatusReported$ = this.removeStatusReportedSource.asObservable();

    // Service message commands
    raiseError(errorMessage: string) {
        this.errorRaisedSource.next(errorMessage);
      }

    reportSuccess(successMessage: string) {
        this.successReportedSource.next(successMessage);
      }

    removeStatus() {
        this.removeStatusReportedSource.next();
    }
}
