"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var testee_component_1 = require("./testee.component");
var assignedQuizzes_component_1 = require("../quiz/assignedQuizzes.component");
var quizViewer_component_1 = require("../quiz/quizViewer.component");
var quizScore_component_1 = require("../quiz/quizScore.component");
var quizViewer_guard_1 = require("../quiz/quizViewer.guard");
var testeeRoutes = [
    { path: '', component: testee_component_1.TesteeComponent,
        children: [
            { path: '', redirectTo: '/testee/assignedQuizzes', pathMatch: 'full' },
            { path: 'assignedQuizzes', component: assignedQuizzes_component_1.AssignedQuizzesComponent },
            { path: 'quizViewer/:quiz_Id', component: quizViewer_component_1.QuizViewerComponent, canDeactivate: [quizViewer_guard_1.QuizViewerGuard] },
            { path: 'quizScore', component: quizScore_component_1.QuizScoreComponent },
        ],
    }
];
var TesteeRoutingModule = (function () {
    function TesteeRoutingModule() {
    }
    return TesteeRoutingModule;
}());
TesteeRoutingModule = __decorate([
    core_1.NgModule({
        imports: [router_1.RouterModule.forChild(testeeRoutes)],
        exports: [router_1.RouterModule]
    })
], TesteeRoutingModule);
exports.TesteeRoutingModule = TesteeRoutingModule;
//# sourceMappingURL=testee-routing.module.js.map