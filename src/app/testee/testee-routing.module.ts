import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TesteeComponent } from './testee.component';
import { AssignedQuizzesComponent } from '../quiz/assignedQuizzes.component';
import { QuizViewerComponent } from '../quiz/quizViewer.component';
import { QuizScoreComponent } from '../quiz/quizScore.component';
import { QuizViewerGuard } from '../quiz/quizViewer.guard';


const testeeRoutes: Routes = [
    { path: '', component: TesteeComponent,
        children:
        [
            { path: '', redirectTo: '/testee/assignedQuizzes', pathMatch: 'full'},
            { path: 'assignedQuizzes', component: AssignedQuizzesComponent },
            { path: 'quizViewer/:quiz_Id', component: QuizViewerComponent , canDeactivate: [ QuizViewerGuard ]},
            { path: 'quizScore', component: QuizScoreComponent },
        ],
    }
  ];

@NgModule({
    imports: [ RouterModule.forChild(testeeRoutes) ],
    exports: [ RouterModule ]
})
export class TesteeRoutingModule { }
