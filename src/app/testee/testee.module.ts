import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { TesteeRoutingModule } from './testee-routing.module';
import { HttpModule } from '@angular/http';
import { TesteeComponent } from './testee.component';
import { AssignedQuizzesComponent } from '../quiz/assignedQuizzes.component';
import { QuizViewerComponent } from '../quiz/quizViewer.component';
import { QuizScoreComponent } from '../quiz/quizScore.component';

@NgModule({
    imports: [ FormsModule, CommonModule, TesteeRoutingModule, HttpModule ],
    declarations: [TesteeComponent, AssignedQuizzesComponent, QuizViewerComponent, QuizScoreComponent ],
})
export class TesteeModule { }
