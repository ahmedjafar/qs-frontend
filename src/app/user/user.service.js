"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var http_2 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
require("rxjs/add/operator/map");
require("rxjs/add/operator/catch");
require("rxjs/add/Observable/throw");
require("rxjs/rx");
var UserService = (function () {
    function UserService(_http) {
        this._http = _http;
    }
    UserService.prototype.getToken = function (userName, password) {
        var headers = new http_2.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this._http.post('http://localhost:9768/token', "username=" + userName + "&password=" + password + "&grant_type=password", options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.getUserRole = function (userName, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        console.log(headers);
        return this._http.get("http://localhost:9768/api/AspNetUserRole?username=" + userName, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.getUserInfo = function (userName, token) {
        var headers = new http_2.Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', "Bearer " + token);
        var options = new http_1.RequestOptions({ headers: headers });
        console.log(headers);
        return this._http.get("http://localhost:9768/api/AspNetUserInfo?username=" + userName, options)
            .map(function (response) { return response.json(); })
            .catch(this.handleError);
    };
    UserService.prototype.handleError = function (error) {
        // console.error(error);
        return Observable_1.Observable.throw(error);
    };
    return UserService;
}());
UserService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_2.Http])
], UserService);
exports.UserService = UserService;
//# sourceMappingURL=user.service.js.map