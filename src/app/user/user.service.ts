import { Injectable } from '@angular/core';
import {RequestOptions} from '@angular/http';
import { Http, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { IUserInfo } from './user';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/Observable/throw';
import 'rxjs/rx';

@Injectable()
export class UserService {
    constructor (private _http: Http) { }

    getToken(userName: string, password: string): Observable<Response> {
        let headers = new Headers({'Content-Type': 'application/x-www-form-urlencoded'});
        let options = new RequestOptions({ headers: headers } );
        return this._http.post('http://localhost:9768/token', `username=${userName}&password=${password}&grant_type=password`, options)
        .map((response: Response) => response.json())
        .catch(this.handleError);
    }

    getUserRole(userName: string, token: string): Observable<string> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        console.log(headers);
        return this._http.get(`http://localhost:9768/api/AspNetUserRole?username=${userName}`, options)
        .map((response: Response) => <string>response.json())
        .catch(this.handleError);
    }

    getUserInfo(userName: string, token: string): Observable<IUserInfo> {
        let headers = new Headers();
        headers.append('Accept', 'application/x-www-form-urlencoded');
        headers.append('Authorization', `Bearer ${token}`);
        let options = new RequestOptions({headers: headers});
        console.log(headers);
        return this._http.get(`http://localhost:9768/api/AspNetUserInfo?username=${userName}`, options)
        .map((response: Response) => <IUserInfo>response.json())
        .catch(this.handleError);
    }

    handleError(error: Response) {
        // console.error(error);
        return Observable.throw(error);
    }
}
