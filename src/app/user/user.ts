export interface IUser {
    Id: number;
    UserName: string;
    Email: string;
    Password: string;
}

export interface IUserInfo {
    UserId: string;
    Role: string;
}
