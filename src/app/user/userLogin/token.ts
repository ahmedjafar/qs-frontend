export interface IToken {
    expires: Date;
    issued: Date;
    access: Date;
    token_type: string;
    userName: string;
}
