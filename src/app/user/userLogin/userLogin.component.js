"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var user_service_1 = require("../user.service");
var UserLoginComponent = (function () {
    function UserLoginComponent(_userService, router) {
        this._userService = _userService;
        this.router = router;
        this.token = localStorage.getItem('token');
        if (this.token != null) {
            var role = localStorage.getItem('role');
            if (role === 'Admin') {
                this.router.navigateByUrl('/admin/quizzesList');
            }
            else if (role === 'Testee') {
                this.router.navigateByUrl('/testee/assignedQuizzes');
            }
        }
    }
    UserLoginComponent.prototype.ngOnInit = function () { };
    UserLoginComponent.prototype.onLoginClick = function () {
        var _this = this;
        this._userService.getToken(this.userName, this.password)
            .subscribe(function (response) {
            _this.token = response.access_token;
            localStorage.setItem('token', _this.token);
            _this._userService.getUserInfo(_this.userName, localStorage.getItem('token'))
                .subscribe(function (userInfo) {
                localStorage.setItem('role', userInfo.Role);
                localStorage.setItem('userId', userInfo.UserId);
                if (userInfo.Role === 'Admin') {
                    _this.router.navigateByUrl('/admin/quizzesList')
                        .then(function (nav) { }, function (error) { });
                }
                else if (userInfo.Role === 'Testee') {
                    _this.router.navigateByUrl('/testee/assignedQuizzes')
                        .then(function (nav) { }, function (error) { });
                }
            }, function (error2) {
                _this.statusMessage = 'Error getting user role!';
                _this.showError = true;
            });
        }, function (error) {
            if (error.status === 400) {
                _this.statusMessage = 'Invalid user name or password!';
            }
            else {
                _this.statusMessage = 'Error getting user credintials!';
            }
            _this.password = '';
            _this.showError = true;
        });
    };
    return UserLoginComponent;
}());
UserLoginComponent = __decorate([
    core_1.Component({
        selector: 'user-login',
        templateUrl: 'app/user/userLogin/userLogin.component.html',
    }),
    __metadata("design:paramtypes", [user_service_1.UserService, router_1.Router])
], UserLoginComponent);
exports.UserLoginComponent = UserLoginComponent;
//# sourceMappingURL=userLogin.component.js.map