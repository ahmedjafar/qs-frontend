import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../user.service';
import { Response } from '@angular/http';

@Component({
    selector: 'user-login',
    templateUrl: 'app/user/userLogin/userLogin.component.html',
})
export class UserLoginComponent implements OnInit {
    userName: string;
    password: string;
    token: string;
    showError: boolean;
    statusMessage: string;

    constructor(private _userService: UserService, private router: Router) {
        this.token = localStorage.getItem('token');
        if (this.token != null) {
            let role: string = localStorage.getItem('role');
            if ( role === 'Admin') {
                this.router.navigateByUrl('/admin/quizzesList');
            } else if (role === 'Testee') {
                this.router.navigateByUrl('/testee/assignedQuizzes');
            }
        }
    }

    ngOnInit() { }

    onLoginClick(): void {
        this._userService.getToken(this.userName, this.password)
            .subscribe(
            (response: any) => {
                this.token = response.access_token;
                localStorage.setItem('token', this.token);
                this._userService.getUserInfo(this.userName, localStorage.getItem('token'))
                    .subscribe(
                    (userInfo) => {
                        localStorage.setItem('role', userInfo.Role);
                        localStorage.setItem('userId', userInfo.UserId);
                        if (userInfo.Role === 'Admin') {
                            this.router.navigateByUrl('/admin/quizzesList')
                                .then(
                                nav => { },
                                error => { });
                        } else if (userInfo.Role === 'Testee') {
                            this.router.navigateByUrl('/testee/assignedQuizzes')
                                .then(
                                nav => { },
                                error => { });
                        }
                    },
                    (error2) => {
                        this.statusMessage = 'Error getting user role!';
                        this.showError = true;
                    });
            },
            (error: Response) => {
                if (error.status === 400) {
                    this.statusMessage = 'Invalid user name or password!';
                } else {
                    this.statusMessage = 'Error getting user credintials!';
                }
                this.password = '';
                this.showError = true;
            }
            );
    }
}
